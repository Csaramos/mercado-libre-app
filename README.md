# Test Mercado libre

Aplicación sencilla de búsqueda y visualización detallada de productos.


### Pre-requisitos 📋

* node v12.18.3 o superior


## Comenzando 🚀

### PASO 1: Clonar e instalar dependencias

```
git clone https://Csaramos@bitbucket.org/Csaramos/mercado-libre-app.git 

cd mercado-libre-app

npm install

```

## Metodologias usadas 📐

### `Estilos`
*BEM (Block Element Modifier)*

Se utilizo esta metodología para tener una nomenclatura consistente en cuanto a estilos. También se escogió teniendo en cuenta que la prueba pide escalabilidad, por lo cual esta metodología tiene una estructura pensada para reutilizacion de componentes, lo que permite una mantenibilidad mas sencilla en proyectos grandes.


## Scripts disponibles

Los siguientes códigos se pueden correr desde el repositorio

### `npm start`

Ejecuta la aplicación en modo desarrollo.

Abre el siguiente link [http://localhost:3000](http://localhost:3000) para visualizar la aplicación.

### `npm run build`

Construye la aplicación en modo productivo.

La app construida queda almacenada en la carpeta build


### `npm run eject`



**La ejecución de este comando no puede ser reservado**

Crea una copia de los archivos de configuración y las dependencias transitivas directamente en el proyecto, lo que permite tener control absoluto sobre las mismas.
## Construido con 🛠️

* [React JS](https://es.reactjs.org/docs/getting-started.html) - El framework web usado
* [SASS](https://sass-lang.com/documentation) - Preprocesador de estilos

## Estructura de carpetas 📁

```
mercado-libre-app/
  README.md
  node_modules/
  public/
  src/
    assets/
      constats/
      images/
      styles/
    components/
    pages/
    utils/
```

## Versión 📌

* 1.0.0

## Autor ✒️

* **Cesar Ramos** - *Autor* - [BitBucket](https://bitbucket.org/Csaramos)
