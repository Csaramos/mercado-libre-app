import "./BreadCrumb.scss";

const BreadCrumb = ({ categories }) => {
  let path;
  const pathCreator = (total, { name }) => {
    return `${total} ${name} >`;
  };
  if (Array.isArray(categories)) {
    const sortCategories = categories
      .slice(0, 4)
      .sort((a, b) => b.results - a.results);
    path = sortCategories.reduce(pathCreator, "");
    path = path.slice(0, -1);
  } else {
    path = categories;
  }
  return <div className="breadCrumb">{path}</div>;
};

export default BreadCrumb;
