import "./Header.scss";
import Logo_ML from "../../assets/images/header/Logo_ML.png";
import SearchBar from "../SearchBar/SearchBar";
import { useHistory } from "react-router-dom";

const Header = ({ clearBreadcrumb }) => {
  const history = useHistory();

  const handleOnClick = () => {
    clearBreadcrumb([]);
    history.push("/");
  };

  return (
    <header className="header">
      <img
        className="header__img"
        alt="logo-mercado-libre"
        src={Logo_ML}
        onClick={handleOnClick}
      />
      <SearchBar />
    </header>
  );
};

export default Header;
