import "./SearchBar.scss";
import ic_Search from "../../assets/images/searchBar/ic_Search.png";
import { useHistory } from "react-router";
import { useState } from "react";

const SearchBar = () => {
  const history = useHistory();
  const [inputValue, setInputValue] = useState("");

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      redirectToProductsGrid();
    }
  };

  const redirectToProductsGrid = () => {
    if (inputValue) {
      clearInput();
      history.push({
        pathname: "/items",
        search: `?search=${inputValue}`,
      });
    }
  };

  const setValue = ({ target }) => {
    setInputValue(target.value);
  };

  const clearInput = () => {
    setInputValue("");
  };

  return (
    <label className="label">
      <input
        className="label__input"
        id="searchBar"
        type="text"
        placeholder="Nunca dejes de buscar"
        onKeyPress={handleKeyPress}
        onChange={setValue}
        value={inputValue}
      ></input>
      <img
        className="label__img"
        alt="icono-buscador"
        src={ic_Search}
        onClick={redirectToProductsGrid}
      />
    </label>
  );
};

export default SearchBar;
