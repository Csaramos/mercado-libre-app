import Header from "./components/Header/Header";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/Home/Home";
import GridItems from "./pages/GridItems/GridItems";
import ItemDetail from "./pages/ItemDetail/ItemDetail";
import Error from "./pages/Error/Error";
import BreadCrumb from "./components/BreadCrumb/BreadCrumb";
import { useState } from "react";

function App() {
  const [categories, setCategories] = useState([])

  return (
    <Router>
      <Header clearBreadcrumb={setCategories}/>
      <div className="container">
        <BreadCrumb categories={categories}/>
        <section className="container__section">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/items">
              <GridItems setBreadcrumb={setCategories}/>
            </Route>
            <Route exact path="/items/:id" component={ItemDetail} />
            <Route exact path="/error" component={Error} />
          </Switch>
        </section>
      </div>
    </Router>
  );
}

export default App;
