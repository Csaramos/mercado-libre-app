import "./Item.scss";
import defaultImage from "../../../../assets/images/gridItem/image-not-found.jpg";
import shippingMark from "../../../../assets/images/gridItem/ic_shipping.png";
import { useHistory } from "react-router-dom";
import setMoneyCurrency from "../../../../utils/setMoneyCurrency";

const Item = ({
  imageSrc,
  description,
  location,
  price,
  isFreeShipping,
  itemId
}) => {
  const history = useHistory();

  const handleOnClick = () => {
    history.push({
      pathname: `/items/${itemId}`,
    });
  };

  return (
    <li className="item" onClick={handleOnClick}>
      <img
        className="item__productImage"
        alt="imagen-del-producto"
        src={imageSrc || defaultImage}
        loading="lazy"
      />
      <section className="item__section">
        <div className="item__price">
          <span>{setMoneyCurrency(price)}</span>
          {isFreeShipping &&
            <img
              className="item__shippingMark"
              alt="imagen-envio-incluido"
              src={shippingMark}
            />
          }
        </div>
        <p className="item__description">{description}</p>
      </section>
      <span className="item__location">{location}</span>
    </li>
  );
};

export default Item;
