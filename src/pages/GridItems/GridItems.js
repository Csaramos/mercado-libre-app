import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router";
import Item from "./components/Item/Item";
import * as Constants from "../../assets/constants/constants";

const GridItems = ({ setBreadcrumb }) => {
  const inputSearch = new URLSearchParams(useLocation().search).get("search");
  const [itemsList, setItemsList] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(
          `${Constants.API_URL}sites/MLA/search?q=${inputSearch}`
        );

        const { results, available_filters } = await response.json();
        const categories =
          available_filters.find(({id}) => id === "category")?.values || inputSearch;
        setItemsList(results.slice(0, 4));
        setBreadcrumb(categories);
      } catch (error) {
        console.log(`Hubo un error: ${error}`);
        history.push({
          pathname: `/error`,
        });
      }
    };
    fetchData();
  }, [inputSearch]);

  return (
    <ul>
      {itemsList.map(
        ({
          id,
          thumbnail,
          title,
          price,
          address: { state_name },
          shipping: { free_shipping },
        }) => (
          <Item
            key={id}
            imageSrc={thumbnail}
            description={title}
            location={state_name}
            price={price}
            isFreeShipping={free_shipping}
            itemId={id}
          />
        )
      )}
    </ul>
  );
};

export default GridItems;
