import defaultImage from "../../assets/images/itemDetail/defaultImageL.jpg";
import "./ItemDetail.scss";
import { useHistory, useParams } from "react-router";
import { useEffect, useState } from "react";
import setItemState from "../../utils/setItemState";
import setMoneyCurrency from "../../utils/setMoneyCurrency";
import * as Constants from "../../assets/constants/constants";

const ItemDetail = () => {
  const { id } = useParams();
  const [itemInfo, setItemInfo] = useState({});
  const history = useHistory();

  useEffect(() => {
    const fetchItemInfo = async () => {
      try {
        let [responseItem, responseDescription] = await Promise.all([
          fetch(`${Constants.API_URL}items/${id}`),
          fetch(`${Constants.API_URL}items/${id}/description`),
        ]);

        const { condition, available_quantity, title, price, pictures } =
          await responseItem.json();

        const { plain_text } = await responseDescription.json();
        const resultInfo = {
          condition,
          available_quantity,
          itemName: title,
          price,
          picture: pictures[0].url,
          description: plain_text,
        };
        setItemInfo(resultInfo);
      } catch (error) {
        console.log(`Hubo un error: ${error}`);
        history.push({
          pathname: `/error`,
        });
      }
    };
    fetchItemInfo();
  }, []);

  const {
    picture,
    condition,
    available_quantity,
    itemName,
    price,
    description,
  } = itemInfo;

  return (
      <div className="container__section-padding">
        <div className="mainContainer">
          <img
            className="mainContainer__productImg"
            alt="imagen-del-producto"
            src={picture || defaultImage}
          />
          <div className="mainContainer__info">
            <p className="mainContainer__condition">
              {setItemState(condition)} - {available_quantity}
            </p>
            <strong className="mainContainer__name">{itemName}</strong>
            <strong className="mainContainer__price">
              {setMoneyCurrency(price)}
            </strong>
            <button className="button button-primary">Comprar</button>
          </div>
        </div>
        <div className="descriptionContainer">
          <h3 className="descriptionContainer__title">
            Descripción del producto
          </h3>
          <p className="descriptionContainer__text">{description}</p>
        </div>
      </div>
  );
};

export default ItemDetail;
