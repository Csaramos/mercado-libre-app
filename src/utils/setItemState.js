const setItemState = (stateCode) => {
  if (stateCode) {
    const value = stateCode.toLowerCase();
    switch (value) {
      case "used":
        return "Usado";
      case "new":
        return "Nuevo";
      default:
          return "No definido"
    }
  }
};

export default setItemState;
