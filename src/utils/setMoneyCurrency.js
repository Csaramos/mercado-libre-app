const setMoneyCurrency = (number) => {
    if (number) {
      const numberChanged = number.toString().replace(".", ",");
    return `$ ${numberChanged.replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".")}`;
  }
};

export default setMoneyCurrency;
